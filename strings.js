/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// return matching position within values or values.length if not found.
function BinarySearch(values, value_to_find)
{
    var start = 0;
    var end;
    var mid;

    if (values.length > 0)
    {
        end = values.length - 1;

        do
        {
            mid = (start + end) >> 1;
            var to_compare = values[mid];
            if (value_to_find < to_compare)
                end = mid - 1;
            else if (value_to_find > to_compare)
                start = mid + 1;
            else
                break;
        }
        while (start <= end);
        return (start <= end) ? mid : values.length;
    }

    return 0;
}

function IsAsciiWhiteSpace(ch)
{
    if (ch.length > 0)
    {
        if (ch.charAt(0) == " " || ch.charAt(0) == "\t")
            return true;
        if (typeof IsAsciiWhiteSpace.lookup === 'undefined')
        {
            IsAsciiWhiteSpace.lookup = [
                "\b".charCodeAt(0), "\n".charCodeAt(0), "\v".charCodeAt(0),
                "\f".charCodeAt(0), "\r".charCodeAt(0),
            ];
        }

        return BinarySearch(IsAsciiWhiteSpace.lookup, ch.charCodeAt(0)) < IsAsciiWhiteSpace.lookup.length;
    }
    return false;
}

// Support Unicode white space characters.
function IsWhiteSpace(ch)
{
    if (ch.length > 0)
    {
        if (typeof IsWhiteSpace.lookup === 'undefined')
        {
            IsWhiteSpace.lookup = [
                "\b".charCodeAt(0), "\t".charCodeAt(0), "\n".charCodeAt(0),
                "\v".charCodeAt(0), "\f".charCodeAt(0), "\r".charCodeAt(0), " ".charCodeAt(0),
                "\u00A0".charCodeAt(0), "\u1680".charCodeAt(0), "\u180E".charCodeAt(0),
                "\u2000".charCodeAt(0), "\u2001".charCodeAt(0), "\u2002".charCodeAt(0),
                "\u2003".charCodeAt(0), "\u2004".charCodeAt(0), "\u2005".charCodeAt(0),
                "\u2006".charCodeAt(0), "\u2007".charCodeAt(0), "\u2008".charCodeAt(0),
                "\u2009".charCodeAt(0), "\u200a".charCodeAt(0), "\u200b".charCodeAt(0),
                "\u202f".charCodeAt(0), "\u205f".charCodeAt(0), "\u3000".charCodeAt(0),
                "\ufeff".charCodeAt(0)
            ];
        }

        // Attempt most common white space first.
        return BinarySearch(IsWhiteSpace.lookup, ch.charCodeAt(0)) < IsWhiteSpace.lookup.length;
    }
    return false;
}

function AsciiTrimLeft(str)
{
    var i = 0;
    while (i < str.length)
    {
        if (!IsAsciiWhiteSpace(str.charAt(i)))
            break;
        i++;
    }
    if (i > 0)
    {
        if (i < str.length)
            return str.substr(i);
        return "";
    }
    return str;
}

function AsciiTrimRight(str)
{
    var i = str.length;
    while (i > 0)
    {
        i--;
        if (!IsAsciiWhiteSpace(str.charAt(i)))
        {
            i++;
            break;
        }
    }
    if (i < str.length)
    {
        if (i > 0)
            return str.substr(0, i);
        return "";
    }
    return str;
}

function TrimLeft(str)
{
    var i = 0;
    while (i < str.length)
    {
        if (!IsWhiteSpace(str.charAt(i)))
            break;
        i++;
    }
    if (i > 0)
    {
        if (i < str.length)
            return str.substr(i);
        return "";
    }
    return str;
}

function TrimRight(str)
{
    var i = str.length;
    while (i > 0)
    {
        i--;
        if (!IsWhiteSpace(str.charAt(i)))
        {
            i++;
            break;
        }
    }
    if (i < str.length)
    {
        if (i > 0)
            return str.substr(0, i);
        return "";
    }
    return str;
}

function TrimLeftRegEx(str)
{
    // Different browsers perform regular expressions with differing performance,
    // so this may be faster or slower than TrimLeft.
    return str.replace(/(^[\t\n\r\v\f \u00a0\u1680\u180e\u2000-\u200a\u2028\u2029\u202f\u205f\u3000\ufeff]*)/g, '');
}
